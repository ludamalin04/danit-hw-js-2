'use strict'

let name;
let age;

do {
    name = prompt('What is your name?');
} while (name.length === 0);

do {
    age = +prompt('What is your age?');
} while (isNaN(age) || !age);

if (isNaN(age)) {
    prompt('What is your age?');
} else if (age < 18) {
    alert('You are not allowed to visit this website.');
} else if (age >= 18 && age <= 22) {
    if (confirm('Are you sure you want to continue?')) {
        alert('Welcome ' + name);
    } else alert('You are not allowed to visit this website.');
} else if (age > 22) {
    alert('Welcome ' + name);
}